package com.apiweb.apiweb.view.controller;

import com.apiweb.apiweb.core.repository.ProdutoRepository;
import com.apiweb.apiweb.model.Produto;
import com.apiweb.apiweb.view.ProdutoRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/produto")
public class ProdutoController {

        @Autowired
        private ProdutoRepository produtoRepository;


        @RequestMapping(method = RequestMethod.GET)
        public ResponseEntity<List<Produto>> listar(){

            List<Produto> produtosOp = produtoRepository.findAll();

            if(produtosOp.isEmpty())
            {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }

            return new ResponseEntity<>(produtosOp, HttpStatus.OK);
        }

        @RequestMapping(path = "{id}", method = RequestMethod.GET)
        public ResponseEntity<Produto> listar(@PathVariable String id){

            Optional<Produto> produtoOp = produtoRepository.findById(id);

            if(!produtoOp.isPresent())
            {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }

            return new ResponseEntity<>(produtoOp.get(), HttpStatus.OK);
        }

        @RequestMapping(path = "/nome/{nome}", method = RequestMethod.GET)
        public ResponseEntity<Produto> buscarPorNome(@PathVariable String nome){

            Produto produto = produtoRepository.findByNome(nome);

            if(produto != null)
            {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }

            return new ResponseEntity<>(produto, HttpStatus.OK);
        }

        @RequestMapping(method = RequestMethod.POST)
        public ResponseEntity<Void> criar(@RequestBody @Valid ProdutoRequest request){

            Produto produto = new Produto();
            produto.setNome(request.getNome());
            produto.setDescricao(request.getDescricao());
            produto.setQuantidade(request.getQuantidade());
            produto.setValor(request.getValor());

            produtoRepository.save(produto);
            return new ResponseEntity<>(HttpStatus.CREATED);
        }

        @RequestMapping(path = "{id}", method = RequestMethod.DELETE)
        public ResponseEntity<Void> deletar(@PathVariable String id){
            Optional<Produto> produtoOp = produtoRepository.findById(id);

            if(!produtoOp.isPresent()) {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
            produtoRepository.delete(produtoOp.get());
            return new ResponseEntity<>(HttpStatus.OK);
        }

    }
