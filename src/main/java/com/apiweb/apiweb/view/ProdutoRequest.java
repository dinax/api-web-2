package com.apiweb.apiweb.view;

import lombok.Data;

@Data
public class ProdutoRequest {
    private String id;
    private String nome;
    private String descricao;
    private int quantidade;
    private Double valor;
}
