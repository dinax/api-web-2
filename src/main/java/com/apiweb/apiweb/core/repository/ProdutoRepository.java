package com.apiweb.apiweb.core.repository;

import com.apiweb.apiweb.model.Produto;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface ProdutoRepository extends MongoRepository<Produto, String> {

    Produto findByNome(String nome);

}
