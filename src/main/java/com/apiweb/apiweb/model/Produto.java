package com.apiweb.apiweb.model;


import lombok.Data;
import org.springframework.data.mongodb.core.mapping.Document;

@Data
@Document
public class Produto {

    private String id;
    private String nome;
    private String descricao;
    private int quantidade;
    private Double valor;


}
